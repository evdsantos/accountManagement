﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// As informações gerais sobre um assembly são controladas por meio do conjunto
// de atributos a seguir. Altere esses valores de atributo para modificar as informações
// associadas a um assembly.
[assembly: AssemblyTitle("Contactmanagement")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Microsoft")]
[assembly: AssemblyProduct("Contactmanagement")]
[assembly: AssemblyCopyright("Copyright © Microsoft 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Se ComVisible estiver definido como false, os tipos neste assembly não estarão visíveis
// para componentes COM.  Se você precisar acessar um tipo neste assembly a partir de
// COM, defina o atributo ComVisible como true nesse tipo.
[assembly: ComVisible(false)]

// O GUID a seguir é para a ID do typelib se este projeto estiver exposto a COM
[assembly: Guid("a902a8f9-8a2d-44f9-8f23-6877f5aebc96")]

// As informações da versão de um assembly consistem nestes quatro valores:
//
//      Versão principal
//      Versão secundária 
//      Número da compilação
//      Revisão
//
// Você pode especificar todos os valores ou usar como padrão os números de compilação e revisão.
// Para isso, use o caractere '*', como a seguir:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
