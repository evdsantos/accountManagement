﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Contactmanagement
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public IOrganizationService _service;
        #region Tarefa: Criar um método nomeado GetOrganization para obter o URI de OrganizationService correto
        public IOrganizationService GetOrganization()
        {
            // OFFICE 365 (ONLINE)
            var connectionString = "Url=https://trialalfa.crm2.dynamics.com; Username=evsantos@trialalfa.onmicrosoft.com; Password=Pumpit1988; authtype=Office365";
            // Connect to the CRM web service using a connection string.
            CrmServiceClient conn = new Microsoft.Xrm.Tooling.Connector.CrmServiceClient(connectionString);
            // Cast the proxy client to the IOrganizationService interface.
            var _service = (IOrganizationService)conn.OrganizationWebProxyClient != null ? (IOrganizationService)conn.OrganizationWebProxyClient : (IOrganizationService)conn.OrganizationServiceProxy;

            return _service;
        }
        #endregion

        public void LoadList(ComboBox cb)
        {
            IList<GenericComboBox> data = new List<GenericComboBox>();
            if (cb.Name == "comboBox1")
            {
                #region 1) Tarefa: Começar a adicionar código para popular a caixa suspensa de contas
                /*
                 select nome, ano from cursos
                 where ano between 2014 and 2016
                 order by ano desc,nome;
                 */
                ColumnSet cs = new ColumnSet();
                cs.AddColumns("name", "accountid");

                QueryExpression qe = new QueryExpression();
                qe.ColumnSet = cs;
                qe.EntityName = "account";

                ConditionExpression ce = new ConditionExpression();
                ce.AttributeName = "statecode";
                ce.Operator = ConditionOperator.Equal;
                ce.Values.Add("Active");

                FilterExpression fe = new FilterExpression();
                fe.AddCondition(ce);
                
                qe.Criteria = fe;
                qe.AddOrder("name", OrderType.Ascending);

                EntityCollection accountlist = _service.RetrieveMultiple(qe);

                foreach (Entity act in accountlist.Entities)
                {
                    var _name = "";
                    //importante verficar se o retrieve trouxe os atributos
                    if (act.Attributes.Contains("name")) _name = act.Attributes["name"].ToString();
                    data.Add(new GenericComboBox(act.Attributes["accountid"].ToString(), _name));
                }
                #endregion
            }

            cb.DataSource = data;
            cb.ValueMember = "Value";

        }
        public void LoadList(GenericComboBox g, ComboBox combobox)
        {

            IList<GenericComboBox> data = new List<GenericComboBox>();
            #region 2) Tarefa: Começar a adicionar código para popular a caixa suspensa de contas

            QueryExpression qe = new QueryExpression();
            ColumnSet cs = new ColumnSet();
            FilterExpression fe = new FilterExpression();
            ConditionExpression ce = new ConditionExpression();
            cs.AddColumns("name", "accountid");
            ce.AttributeName = "statecode";
            ce.Operator = ConditionOperator.Equal;
            ce.Values.Add("Active");
            fe.AddCondition(ce);
            qe.ColumnSet = cs;
            qe.Criteria = fe;
            qe.EntityName = "account";
            qe.AddOrder("name", OrderType.Ascending);
            EntityCollection accountlist = _service.RetrieveMultiple(qe);
            foreach (Entity act in accountlist.Entities)
            {
                data.Add(new GenericComboBox(act.Attributes["accountid"].ToString(), act.Attributes["name"].ToString()));
            }

            #endregion


            combobox.DataSource = data;
            combobox.ValueMember = "Valor";
            combobox.SelectedItem = data.First(GC => GC.Key == g.Key);

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            _service = GetOrganization(); 

            LoadList(comboBox1);
        }

        #region Código usado para processamento interno
        public class GenericComboBox
        {
            private string _key = string.Empty;
            private string _value = string.Empty;
            public string Key { get { return _key; } set { _key = value; } }
            public string Value { get { return _value; } set { _value = value; } }
            public GenericComboBox(string key, string value)
            { _key = key; _value = value; }
        }
        #endregion

        private void btnCreate_Click(object sender, EventArgs e)
        {

            #region TAREFA: Adicionar código aqui
            Entity account = new Entity("account");
            account.Attributes["name"] = this.txtName.Text;
            account.Attributes["address1_line1"] = this.txtAddressLine1.Text;
            account.Attributes["address1_city"] = this.txtCity.Text;
            account.Attributes["address1_stateorprovince"] = this.txtState.Text;
            Guid g = _service.Create(account);
            LoadList(comboBox1);
            #endregion

        }


        private void btnUpdate_Click(object sender, EventArgs e)
        {
            #region TAREFA: Adicionar código aqui
            GenericComboBox g = (GenericComboBox)comboBox1.SelectedItem;
            Entity account = new Entity("account");
            account.Attributes["accountid"] = new Guid(g.Key);
            account.Attributes["name"] = this.txtName.Text;
            account.Attributes["address1_line1"] = this.txtAddressLine1.Text;
            account.Attributes["address1_city"] = this.txtCity.Text;
            account.Attributes["address1_stateorprovince"] = this.txtState.Text;
            _service.Update(account);
            LoadList(g, comboBox1);
            #endregion

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            #region TAREFA: Adicionar código aqui
            GenericComboBox g = (GenericComboBox)comboBox1.SelectedItem;
            _service.Delete("account", new Guid(g.Key));
            LoadList(comboBox1);
            #endregion


        }
        #region Código usado para avaliar se um atributo existe no registro retornado
        public Boolean AttributeExist(string name, Entity entity)
        {
            Boolean exist = false;

            foreach (var xu in entity.Attributes)
            {
                if (xu.Key == name)
                {

                    exist = true;
                }

            }
            return exist;

        }
        #endregion
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            #region Código para manipular alteração de itens ComboBox1
            this.txtCity.Clear();
            this.txtState.Clear();
            this.txtAddressLine1.Clear();
            this.txtName.Clear();
            IList<GenericComboBox> data = new List<GenericComboBox>();
            GenericComboBox g = (GenericComboBox)comboBox1.SelectedItem;
            #endregion
            #region Tarefa: Recuperar valores necessários para popular os campos de formulário
            ColumnSet cs2 = new ColumnSet(new string[] { "address1_city", "address1_line1", "address1_stateorprovince", "name", "industrycode" });
            var account = _service.Retrieve("account", new Guid(g.Key), cs2);
            if (AttributeExist("name", account))
            {
                this.txtName.Text = account.Attributes["name"].ToString();
            }
            if (AttributeExist("address1_city", account))
            {
                this.txtCity.Text = account.Attributes["address1_city"].ToString();
            }
            if (AttributeExist("address1_line1", account))
            {
                this.txtAddressLine1.Text = account.Attributes["address1_line1"].ToString();
            }
            if (AttributeExist("address1_stateorprovince", account))
            {
                this.txtState.Text = account.Attributes["address1_stateorprovince"].ToString();
            }
            #endregion


        }
    }
               
          }

       
    

