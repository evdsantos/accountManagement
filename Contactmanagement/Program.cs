using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Contactmanagement
{
    static class Program
    {
        /// <summary>
        /// O principal ponto de entrada do aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
